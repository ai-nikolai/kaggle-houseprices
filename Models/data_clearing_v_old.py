##################################################
##################################################
# @Author: Nikolai Rozanov <Nikolai>
# @Date:   28-02-2017
# @Email:  nikolai.rozanov@gmail.com
#
#   (C) Nikolai Rozanov
#
##################################################
##################################################

import numpy as np
import pandas as pd
from sklearn import preprocessing


########################################################################
# Extract relevant feats that are both in train and test data
########################################################################
def get_feats(train, test):
    train_cols = set(train.columns.values)
    train_cols.remove('SalePrice')
    test_cols = set(test.columns.values)
    feats = list(train_cols & test_cols)
    feats.remove('Id')
    return feats

########################################################################
# Deal with missing values
########################################################################
def treat_missing_values(raw_data,cts_cols,cat_cols):

    def miss_freq(data):
        # returns frequency table of missing words for each column (0 freq. not listed)
        miss_cols = data.columns[data.isnull().any()].tolist() # cols with missing values
        return data[miss_cols].isnull().sum()

    if len(miss_freq(raw_data)) == 0:
        return raw_data

    def impute(data,column, value):
        # Impute missing value
        data.loc[data[column].isnull(),column] = value

    # COLUMNS TO TREAT MISSING VALUESS individually:
    bsmt_cols_2 = ['BsmtQual','BsmtCond','BsmtExposure','BsmtFinType1','BsmtFinType2']
    garage_cols = ['GarageType','GarageQual','GarageCond','GarageYrBlt','GarageFinish','GarageCars','GarageArea']
    other_cols = ['LotArea', 'LotFrontage', 'Alley', 'MasVnrArea',
                    'MasVnrType', 'Electrical', 'FireplaceQu', 'PoolQC', 'Fence', 'MiscFeature']

    raw_data = raw_data.assign(logLotArea = lambda x: np.log(x['LotArea']+1e2))
    ind = raw_data['LotFrontage'].isnull()
    raw_data.loc[ind,'LotFrontage'] = raw_data.loc[ind,'logLotArea']
    del raw_data['logLotArea']

    impute(raw_data,'Alley','NA')
    impute(raw_data,'MasVnrArea', 0.)
    impute(raw_data,'MasVnrType','None')
    for col in bsmt_cols_2:
        impute(raw_data,col,'NA')
    impute(raw_data,'Electrical','SBrkr')
    impute(raw_data,'FireplaceQu','NA')
    for col in garage_cols:
        if raw_data[col].dtype == np.object:
            impute(raw_data,col,'NA')
        else:
            impute(raw_data,col, 0)
    impute(raw_data,'PoolQC', 'NA')
    impute(raw_data,'Fence', 'NA')
    impute(raw_data,'MiscFeature', 'NA')

    # Handle the remaining columns
    remaining_cols = list(set(cts_cols + cat_cols) - set(bsmt_cols_2 + garage_cols + other_cols))
    for col in remaining_cols:
        if raw_data[col].dtype ==  'O':
            raw_data[col].fillna('NA', inplace=1)
        else:
            raw_data[col].fillna(0, inplace=1)

    ## CONVERT CTS -> CAT and CAT -> CTS based on common-sense
    #  Note that 'NA' are assumed to be mean values
    raw_data = raw_data.replace({
                            'MSSubClass':{
                                            20:'c20',
                                            30:'c30',
                                            40:'c40',
                                            45:'c45',
                                            50:'c50',
                                            60:'c60',
                                            70:'c70',
                                            75:'c75',
                                            80:'c80',
                                            85:'c85',
                                            90:'c90',
                                            120:'c120',
                                            150:'c150',
                                            160:'c160',
                                            180:'c180',
                                            190:'c190'
                                            },
                            'MSZoning': {'C (all)': 'C'
                                            },
                            'ExterQual': {'Ex': 5,
                                            'Gd': 4,
                                            'TA': 3,
                                            'Fa': 2,
                                            'Po': 1
                                            },
                            'ExterCond': {'Ex': 5,
                                            'Gd': 4,
                                            'TA': 3,
                                            'Fa': 2,
                                            'Po': 1
                                            },
                            'BsmtQual':{
                                            'Ex':5,
                                            'Gd':4,
                                            'TA':3,
                                            'Fa':2,
                                            'Po':1,
                                            'NA': 3},
                            'BsmtCond': {'Ex': 5,
                                            'Gd': 4,
                                            'TA': 3,
                                            'Fa': 2,
                                            'Po': 1,
                                            'NA': 3},
                            'BsmtExposure': {'Gd':4,
                                            'Av':3,
                                            'Mn':2,
                                            'No':1,
                                            'NA':3},
                            'BsmtFinType1':{'GLQ':6,
                                            'ALQ':5,
                                            'BLQ':4,
                                            'Rec':3,
                                            'LwQ':2,
                                            'Unf':1,
                                            'NA':0},
                            'BsmtFinType2':{
                                            'GLQ':6,
                                            'ALQ':5,
                                            'BLQ':4,
                                            'Rec':3,
                                            'LwQ':2,
                                            'Unf':1,
                                            'NA':0},
                            'HeatingQC': {'Ex': 5,
                                            'Gd': 4,
                                            'TA': 3,
                                            'Fa': 2,
                                            'Po': 1
                                            },
                            'KitchenQual': {'Ex': 5,
                                            'Gd': 4,
                                            'TA': 3,
                                            'Fa': 2,
                                            'Po': 1,
                                            'NA':3},
                            'Functional': {'Typ': 8,
                                            'Min1': 7,
                                            'Min2': 6,
                                            'Mod': 5,
                                            'Maj1': 4,
                                            'Maj2': 3,
                                            'Sev': 2,
                                            'Sal': 1,
                                            'NA': 8},
                            'FireplaceQu': {'Ex': 5,
                                            'Gd': 4,
                                            'TA': 3,
                                            'Fa': 2,
                                            'Po': 1,
                                            'NA': 3
                                            },
                            'GarageFinish': {
                                            'Fin':3,
                                            'RFn':2,
                                            'Unf':1,
                                            'NA':0
                                             },
                            'GarageQual': {'Ex': 5,
                                            'Gd': 4,
                                            'TA': 3,
                                            'Fa': 2,
                                            'Po': 1,
                                            'NA': 3},
                            'GarageCond': {'Ex': 5,
                                            'Gd': 4,
                                            'TA': 3,
                                            'Fa': 2,
                                            'Po': 1,
                                            'NA': 3},
                            'PoolQC': {'Ex':4,
                                            'Gd':3,
                                            'TA':2,
                                            'Fa':1,
                                            'NA':2
                                       }
                            })

    assert len(miss_freq(raw_data)) == 0

    return raw_data

########################################################################
# Log-transform skewed continuous feats
########################################################################
def log_transform_skewed(data, cts_cols, tol):
    #log transform skewed numeric feats:
    skewed_cols = data[cts_cols].apply(lambda col: pd.DataFrame.skew(col.dropna()))
    skewed_cols = skewed_cols[abs(skewed_cols) > tol].index
    print('Skewed feats:\n',skewed_cols)
    data[skewed_cols] = np.log1p(data[skewed_cols])
    return data

########################################################################
# Prepare data
########################################################################
def preprocess_feats(train,test):

     #### MANUALLY SPECIFY TYPES OF COLUMNS
    CONTINUOUS_COLUMNS = ["LotFrontage","LotArea","OverallQual","OverallCond","YearBuilt","YearRemodAdd","MasVnrArea","ExterQual","ExterCond",
        "BsmtQual","BsmtCond","BsmtExposure","BsmtFinType1","BsmtFinSF1","BsmtFinType2","BsmtFinSF2","BsmtUnfSF","TotalBsmtSF","HeatingQC",
        "1stFlrSF","2ndFlrSF","LowQualFinSF","GrLivArea","BsmtFullBath","BsmtHalfBath","FullBath","HalfBath","BedroomAbvGr","KitchenAbvGr",
        "KitchenQual","TotRmsAbvGrd","Functional","Fireplaces","FireplaceQu","GarageYrBlt","GarageFinish","GarageCars",
        "GarageArea","GarageQual","GarageCond","WoodDeckSF","OpenPorchSF","EnclosedPorch","3SsnPorch","ScreenPorch","PoolArea","PoolQC",
        "MiscVal","MoSold","YrSold"]
    CATEGORICAL_COLUMNS = ["MSSubClass","MSZoning","Street","Alley","LotShape","LandContour","Utilities","LotConfig","LandSlope","Neighborhood",
        "Condition1","Condition2","BldgType","HouseStyle","RoofStyle","RoofMatl","Exterior1st","Exterior2nd","MasVnrType",
        "Foundation","Heating","CentralAir","Electrical","GarageType","PavedDrive","Fence","MiscFeature","SaleType","SaleCondition"]

    print("Continuous columns {}".format(len(CONTINUOUS_COLUMNS)))
    print("Categorical columns {}".format(len(CATEGORICAL_COLUMNS)))

    print("Treating missing values")
    test = treat_missing_values(test,CONTINUOUS_COLUMNS,CATEGORICAL_COLUMNS)
    train = treat_missing_values(train,CONTINUOUS_COLUMNS,CATEGORICAL_COLUMNS)

    print("Transforming skewed continuous feats - SKEW")
    skew_tol = 1.96 #1.
    test = log_transform_skewed(test, CONTINUOUS_COLUMNS, skew_tol)
    train = log_transform_skewed(train, CONTINUOUS_COLUMNS, skew_tol)

    print("Creating cross feats")
    #TODO

    # categorical_feats = train.select_dtypes(include = ["object"]).columns.values
    print("Encoding categorical feats")
    for col in CATEGORICAL_COLUMNS:
        print(col)
        le = preprocessing.LabelEncoder()
        le.fit(pd.concat([train[col],test[col]],ignore_index = 1))
        train[col] = le.transform(train[col])
        test[col] = le.transform(test[col])

    assert (len(train.select_dtypes(include = ["object"]).columns.values) == 0 &
                len(test.select_dtypes(include = ["object"]).columns.values) == 0)

    print("Extracting final feats")
    feats = get_feats(train,test)

    return train,test,feats
