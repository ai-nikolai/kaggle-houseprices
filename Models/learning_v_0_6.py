import numpy as np
import pandas as pd
import time
import itertools
from sklearn.model_selection import train_test_split, KFold
import xgboost as xgb


def execute_model(train,test,feats,target = 'SalePrice'):
    folds = 2
    random_state = 1

    eta_list = [0.03] # list of parameters to try
    lambdas  = np.linspace(1,2,4)
    alphas   = [0,1e-3,1e-2,1e-1,1]
    max_depth = 3#[2,3,4,5,6] # list of parameters to try


    # eta_list = [0.01] # list of parameters to try
    # max_depth_list = [4] # list of parameters to try

    num_boost_round = 7000 # should be improportional to eta
    early_stopping_rounds = 700
    subsample = 1 # like dropout, recommend no subsampling ( = 1) as we have cross-validation and litle data
    colsample_bytree = 1

    predict_test_by_average_kfold = 1#0
    t0 = time.time()

    # start the training
    scores = np.zeros([len(eta_list),len(lambdas)]) # store score values
    temp_scores = np.zeros(folds)


    kf = KFold(n_splits=folds, random_state=random_state, shuffle=True)
    for idx in range(len(eta_list)):# Loop over parameters to find the better set
        for jdx in range(len(lambdas)):
            print('XGBoost params. ETA: {}, LAMBDAS: {}'.format(eta_list[idx], lambdas[jdx]))
            params = {
                "objective": "reg:linear",
                "booster" : "gbtree", #gblinear, dart
                # "booster" : "gblinear",
                # "booster" : "dart",
                "eval_metric": "rmse", # as defined in kaggle
                "eta": eta_list[idx], # lower parameters tend to prevent overfitting
                "tree_method": 'exact',
                "max_depth": max_depth,
                "subsample": subsample,
                "colsample_bytree": colsample_bytree,
                "silent": 1,
                "seed": 0,
                "lambda": lambdas[jdx],
            }
            i = 0
            for train_index, valid_index in kf.split(train):
                X_train, X_valid  = train[feats].as_matrix()[train_index], train[feats].as_matrix()[valid_index]
                y_train, y_valid  = np.log(train[target].as_matrix()[train_index]), np.log(train[target].as_matrix()[valid_index])

                # Dmatrices for xboost compatibility:
                dtrain = xgb.DMatrix(X_train, y_train)
                dvalid = xgb.DMatrix(X_valid, y_valid)
                # evaluate and print according to watchlist
                watchlist = [(dtrain, 'train'), (dvalid, 'eval')]
                # TRAIN:
                bst = xgb.train(params, dtrain, num_boost_round, evals=watchlist, early_stopping_rounds=early_stopping_rounds, verbose_eval=False) # find the best score


                # score
                temp_scores[i] = bst.best_score
                i = i+1

            scores[idx,jdx] = np.average(temp_scores)
            print('Current Score%f'%(scores[idx,jdx]))


    etax,dx = np.where(scores==scores.min())[0][0], np.where(scores==scores.min())[1][0]


    params['eta'], params['lambda'] = eta_list[etax], lambdas[dx]
    # train again
    dtrain = xgb.DMatrix(train[feats].as_matrix(), np.log(train[target].as_matrix()))
    bst = xgb.train(params, dtrain, num_boost_round, evals= [(dtrain, 'train')], early_stopping_rounds=early_stopping_rounds, verbose_eval=False)

    # train results
    score = scores.min()
    print('Best score: {:.6g}'.format(score))

    print('BEST PARAMS: ETA=%f, LAMBDA=%d'%(eta_list[etax],lambdas[dx]))

    # predict on test
    fin_pred = bst.predict(xgb.DMatrix(test[feats].as_matrix()), ntree_limit=bst.best_ntree_limit)


    # bst.load_model("latestmodel.bin") # load data
    return fin_pred, score, scores







        # importance = sorted(bst.get_fscore().items(), key=itemgetter(1), reverse=1) # importance := bst.get_fscore()
        #     print('Features importance :\n{}'.format(importance))
        #     print('Number of features:', len(feats))
        #     print('Features\n',feats)
