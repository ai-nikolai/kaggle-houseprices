import numpy as np
import pandas as pd
import time
import itertools
from sklearn.model_selection import train_test_split, KFold
import xgboost as xgb
from sklearn import linear_model

def get_score(y_hat,y):
    return np.sqrt(np.inner(y-y_hat,y-y_hat)/len(y))


# def data_feature_mapping(train,test):


def execute_model(train,test,feats,target = 'SalePrice'):
    train_data = train[feats].as_matrx()
    test_data  = test[feats].as_matrix()

    # parameters for sklearn
    n1 = 5
    n2 = 20
    l1_ratio = np.linspace(0,1,n1) #for elastic net
    alphas = np.linspace(0.01,2,n2)
    folds = 4

    # init
    scores = np.zeros([n1,n2])
    temp_scores = np.zeros(folds)
    kf = KFold(n_splits=folds, shuffle=True)

    # timing
    t0 = time.time()

    model = linear_model.ElasticNet(l1_ratio=l1_ratio[0],alpha=alphas[0])
    #  Training and Cross Validation
    for idx in range(n1):
        for jdx in range(n2):
            model.set_params(l1_ratio=l1_ratio[idx],alpha=alphas[jdx])
            # Cross Validation
            i = 0
            for train_index, valid_index in kf.split(train):
                # getting the data
                X_train, X_valid  = train[feats].as_matrix()[train_index], train[feats].as_matrix()[valid_index]
                Y_train, Y_valid  = np.log(train[target].as_matrix()[train_index]), np.log(train[target].as_matrix()[valid_index])
                model.fit(X_train,Y_train)
                temp = model.predict(X_valid)
                temp_scores[i] = get_score(temp,Y_valid)
                i = i + 1

            # computing the average score for this method
            scores[idx,jdx] = np.mean(temp_scores)
            print('Current Params: alpha=%f, l1_ratio=%f; SCORE: %f'%(alphas[jdx],l1_ratio[idx],scores[idx,jdx]))

    # model = linear_model.LinearRegression()




    ###################################
    #



    # Parameters
    params = model.get_params(False)

    # Test Prediction
    fin_pred = model.predict(test_data)

    #mse
    score  = np.min(scores)


    print('Training time: {} min'.format(round((time.time() - t0)/60, 3)))

    # # Save model:
    # bst.save_model('latest.model')
    # bst.dump_model('latestdump.raw.txt','latestfeatmap.txt')

    # # Loading a model #have not tried
    # bst = xgb.Booster({'nthread':4}) #init model
    # bst.load_model("latestmodel.bin") # load data
    return fin_pred, score, scores
