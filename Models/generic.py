##################################################
##################################################
# @Author: Nikolai Rozanov <Nikolai>
# @Date:   28-02-2017
# @Email:  nikolai.rozanov@gmail.com
#
#   (C) Nikolai Rozanov
#
##################################################
##################################################
import csv,datetime


########################################################################
# Save prediction to format desired by Kaggle:
########################################################################

def save_predictions(predictions,test,score):

    filename = 'submission_%s.csv'%(
        datetime.datetime.now().strftime("%Y-%m-%d-%H-%M"))
    prediction_writer = csv.writer( open(filename, 'w', newline='') )
    prediction_writer.writerow(["Id","SalePrice"]) # Header
    for i in range(len(test)):
        prediction_writer.writerow([test.Id[i], predictions[i]])
    print('Submission created: ', filename)
