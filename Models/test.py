import numpy as np
import pandas as pd
from sklearn import preprocessing

from data_clearing_v_1_0 import treat_missing_values

def get_feats(train, test):
    train_cols = set(train.columns.values)
    train_cols.remove('SalePrice')
    test_cols = set(test.columns.values)
    feats = list(train_cols & test_cols)
    feats.remove('Id')
    return feats
############################################################################
# Main code
###########################################################################
if __name__ ==  "__main__":

    train = pd.read_csv("../train.csv")

    CONTINUOUS_COLUMNS = ["LotFrontage","LotArea","YearBuilt","YearRemodAdd","MasVnrArea",
        "BsmtFinSF1","BsmtFinSF2","BsmtUnfSF","TotalBsmtSF",
        "1stFlrSF","2ndFlrSF","LowQualFinSF","GrLivArea","BsmtFullBath","BsmtHalfBath","FullBath","HalfBath","BedroomAbvGr","KitchenAbvGr",
        "TotRmsAbvGrd","Fireplaces","GarageYrBlt","GarageCars",
        "GarageArea","WoodDeckSF","OpenPorchSF","EnclosedPorch","3SsnPorch","ScreenPorch","PoolArea",
        "MiscVal","MoSold","YrSold"]
    CATEGORICAL_COLUMNS = ["MSSubClass","MSZoning","Street","Alley","LotShape","LandContour","Utilities","LotConfig","LandSlope","Neighborhood",
        "Condition1","Condition2","BldgType","HouseStyle","OverallQual","OverallCond","RoofStyle","RoofMatl","Exterior1st","Exterior2nd","MasVnrType",
        "ExterQual","ExterCond","Foundation","KitchenQual","BsmtQual","BsmtCond","BsmtExposure","BsmtFinType1","BsmtFinType2","HeatingQC","Functional","FireplaceQu",
        "Heating","CentralAir","Electrical","GarageType","GarageCond","PavedDrive","Fence","MiscFeature","PoolQC","SaleType","SaleCondition",
        "GarageFinish","GarageQual"]


    data = treat_missing_values(train,CONTINUOUS_COLUMNS,CATEGORICAL_COLUMNS)
    feats = get_feats(train,train)


    for col in CATEGORICAL_COLUMNS:
        le = preprocessing.LabelEncoder()
        le.fit(data[col])
        data[col] = le.transform(data[col])

    temp = data[CATEGORICAL_COLUMNS[0]]

    print(temp[1:3])
    enc = preprocessing.OneHotEncoder(sparse=False)
    temp2 = enc.fit_transform(data[CATEGORICAL_COLUMNS[0]].values.reshape(-1,1))

    temp3 = data[CONTINUOUS_COLUMNS].as_matrix()
    print(temp2[1])
    print(temp3[1])

    temp4 = np.concatenate([temp2,temp3],1)


    print(temp4[1])






        #
