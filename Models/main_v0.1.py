import numpy as np
import pandas as pd
import csv,datetime
from sklearn.model_selection import train_test_split, KFold
import xgboost as xgb
from operator import itemgetter
import time
import matplotlib.pyplot as plt
import seaborn as sns
import itertools
from sklearn import preprocessing


from generic             import save_predictions
from data_clearing_v_old import preprocess_feats
from learning_v_0_3      import execute_model


#TODO:
# removing irrelevant feats (L1 ridge regression -> removing feats with abs(weight)<tolerance )
# cross feats
# even more transformations (based on detailed data analysis)
# hyperparameter optimization/gridSearch

#
#
#

# If above done:
# changing treatment of missing values e.g. substitute 'NA' values differently than by 'mean'/typical values
# (possibly trying out different models)



############################################################################
# Main code
###########################################################################
if __name__ ==  "__main__":

    train = pd.read_csv("../train.csv")
    test = pd.read_csv("../test.csv")

    train,test,feats = preprocess_feats(train,test)
    test_pred, score, scores= execute_model(train,test,feats)

    print(score)

    plt.figure()
    sns.heatmap(scores, annot=False, fmt="6.4f",cmap="YlGnBu")
    # sns.heatmap(scores, annot=True, fmt="6.4f",cmap="YlGnBu")

    plt.show()


    # print(len(train['SalePrice']))
    # print(len(score))
    # print(y_hat-y)
    # print(score)
    # print(params)

    # save_predictions(np.exp(test_pred),test,score)



    #test_pred,score = train_and_test_linear(train,test,feats)
    #test_pred,score = train_and_test_tree(train,test,feats) # run at least once this one to get the feats importance
    #feats = np.load("feats_importance.npy")

# def train_and_test_linear(train,test,feats,target = 'SalePrice'): # simple xgboost
#     subsample = 0.8
#     colsample_bytree = 0.8
#     num_boost_round = 1000 #115 originally
#     early_stopping_rounds = 50
#     test_size = 0.2 # 0.1 originally

#     start_time = time.time()

#     # start the training

#     params = {
#         "objective": "reg:linear",
#         "booster" : "gblinear", #"gbtree",# default
#         "eval_metric": "rmse",
#         "subsample": subsample, # collect 80% of the data only to prevent overfitting
#         "colsample_bytree": colsample_bytree,
#         "silent": 1,
#         "seed": 0,
#     }

#     X_train, X_valid = train_test_split(train, test_size = test_size, random_state = 0) # randomly split into 90% test and 10% CV -> still has the outcome at this point
#     y_train = np.log(X_train[target]) # define y as the outcome column, apply log to have same error as the leaderboard
#     y_valid = np.log(X_valid[target])
#     dtrain = xgb.DMatrix(X_train[feats], y_train) # DMatrix are matrix for xgboost
#     dvalid = xgb.DMatrix(X_valid[feats], y_valid)

#     watchlist = [(dtrain, 'train'), (dvalid, 'eval')] # list of things to evaluate and print
#     bst = xgb.train(params, dtrain, num_boost_round, evals = watchlist, early_stopping_rounds = early_stopping_rounds, verbose_eval = True) # find the best score
#     score = bst.best_score #roc_auc_score(X_valid[target].values, check)
#     print('Last error value: {:.6f}'.format(score))

#     print("Predict test set...")
#     test_pred = bst.predict(xgb.DMatrix(test[feats]))

#     print('Training time: {} minutes'.format(round((time.time() - start_time)/60, 2)))

#     return test_pred, score

# def train_and_test_tree(train,test,feats,target = 'SalePrice'): # simple xgboost
#     eta_list = [0.1,0.2] # list of parameters to try
#     max_depth_list = [4,6,8] # list of parameters to try
#     subsample = 0.8
#     colsample_bytree = 0.8

#     num_boost_round = 400
#     early_stopping_rounds = 10
#     test_size = 0.2

#     start_time = time.time()

#     # start the training
#     array_score = np.ndarray((len(eta_list)*len(max_depth_list),3)) # store score values
#     i = 0
#     for eta,max_depth in list(itertools.product(eta_list, max_depth_list)): # Loop over parameters to find the better set
#         print('XGBoost params. ETA: {}, MAX_DEPTH: {}, SUBSAMPLE: {}, COLSAMPLE_BY_TREE: {}'.format(eta, max_depth, subsample, colsample_bytree))
#         params = {
#             "objective": "reg:linear",
#             "booster" : "gbtree",
#             "eval_metric": "rmse", # as given by kaggle
#             "eta": eta, # shrink against overfitting
#             "tree_method": 'exact',
#             "max_depth": max_depth,
#             "subsample": subsample, # drop-out
#             "colsample_bytree": colsample_bytree,
#             "silent": 1,
#             "seed": 0,
#         }

#         X_train, X_valid = train_test_split(train, test_size = test_size, random_state = 0) # randomly split into 90% test and 10% CV -> still has the outcome at this point
#         y_train = np.log(X_train[target]) # define y as the outcome column
#         y_valid = np.log(X_valid[target])
#         dtrain = xgb.DMatrix(X_train[feats], y_train) # DMatrix are matrix for xgboost
#         dvalid = xgb.DMatrix(X_valid[feats], y_valid)

#         watchlist = [(dtrain, 'train'), (dvalid, 'eval')] # list of things to evaluate and print
#         bst = xgb.train(params, dtrain, num_boost_round, evals = watchlist, early_stopping_rounds = early_stopping_rounds, verbose_eval = True) # find the best score

#         print("Validating...")
#         score = bst.best_score
#         print('Last error value: {:.6f}'.format(score))
#         array_score[i][0] = eta
#         array_score[i][1] = max_depth
#         array_score[i][2] = score
#         i+ = 1
#     df_score = pd.DataFrame(array_score,columns = ['eta','max_depth','SalePrice'])
#     print("df_score : \n", df_score)
#     #create_feature_map(feats)
#     importance = bst.get_fscore()
#     importance = sorted(importance.items(), key = itemgetter(1), reverse = True)
#     print('Importance array: ', importance)
#     np.save("feats_importance",importance) # save feature importance for latter use
#     print("Predict test set...")
#     test_pred = bst.predict(xgb.DMatrix(test[feats]), ntree_limit = bst.best_ntree_limit) # only predict with the last set of parameters

#     print('Training time: {} minutes'.format(round((time.time() - start_time)/60, 2)))

#     return test_pred, score
