import numpy as np
import pandas as pd
import time
import itertools
from sklearn.model_selection import train_test_split, KFold
import xgboost as xgb
from sklearn import linear_model

def get_score(y_hat,y):
    return np.sqrt(np.inner(y-y_hat,y-y_hat)/len(y))



def execute_model(train,train_data,test_data,labels):
    # parameters for sklearn
    l1_ratios = [0.7,0.8,0.9,1]
    alphas    = [0,1,2,5,7,10,30]
    folds = 3

    # init
    n1 = len(l1_ratios)
    n2 = len(alphas)
    scores = np.zeros([n1, n2])
    temp_scores = np.zeros(folds)
    kf = KFold(n_splits=folds, shuffle=True)

    # timing
    t0 = time.time()


    # model = linear_model.Lasso(alpha=alphas[0])
    model = linear_model.ElasticNet(l1_ratio=l1_ratios[0],alpha=alphas[0],max_iter=50000)

    #  Training and Cross Validation
    for idx in range(n1): #l1
        for jdx in range(n2): #alphas
            model.set_params(l1_ratio= l1_ratios[idx],alpha=alphas[jdx])

            i = 0
            for train_index, valid_index in kf.split(train):
                # getting the data
                X_train, X_valid  = train_data[train_index], train_data[valid_index]
                Y_train, Y_valid  = np.log(labels[train_index]), np.log(labels[valid_index])
                model.fit(X_train,Y_train)
                temp = model.predict(X_valid)
                temp_scores[i] = get_score(temp,Y_valid)
                i = i + 1

            # computing the average score for this method
            scores[idx,jdx] = np.mean(temp_scores)
            print('Current  SCORE: %f'%(scores[idx,jdx]))
            print('Current Params: L1=%f, Alpha=%f'%(l1_ratios[idx],alphas[jdx]))
    # model = linear_model.LinearRegression()




    ###################################
    #
    ldx,adx = np.where(scores==scores.min())[0][0], np.where(scores==scores.min())[1][0]
    alpha_best = alphas[adx]
    l1_best    = l1_ratios[ldx]

    #train again
    model.set_params(l1_ratio = l1_best,alpha=alpha_best)
    model.fit(train_data,np.log(labels))

    print('BEST PARAMS: L1=%f, Alpha=%f'%(l1_best,alpha_best))
    # Parameters
    params = model.get_params(False)

    # Test Prediction
    fin_pred = model.predict(test_data)

    #mse
    score  = np.min(scores)

    print('Best score: {:.6g}'.format(score))
    print('Training time: {} min'.format(round((time.time() - t0)/60, 3)))

    # # Save model:
    # bst.save_model('latest.model')
    # bst.dump_model('latestdump.raw.txt','latestfeatmap.txt')

    # # Loading a model #have not tried
    # bst = xgb.Booster({'nthread':4}) #init model
    # bst.load_model("latestmodel.bin") # load data
    return fin_pred, score, scores, model.coef_
