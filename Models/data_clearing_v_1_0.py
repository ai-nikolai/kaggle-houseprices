##################################################
##################################################
# @Author: Nikolai Rozanov <Nikolai>
# @Date:   28-02-2017
# @Email:  nikolai.rozanov@gmail.com
#
#   (C) Nikolai Rozanov
#
##################################################
##################################################

import numpy as np
import pandas as pd
from sklearn import preprocessing



########################################################################
# Extract relevant feats that are both in train and test data
########################################################################
def get_feats(train, test):
    train_cols = set(train.columns.values)
    train_cols.remove('SalePrice')
    test_cols = set(test.columns.values)
    feats = list(train_cols & test_cols)
    feats.remove('Id')
    return feats


########################################################################
# Deal with missing values
########################################################################
def treat_missing_values(raw_data,cts_cols,cat_cols):

    def miss_freq(data):
        # returns frequency table of missing words for each column (0 freq. not listed)
        miss_cols = data.columns[data.isnull().any()].tolist() # cols with missing values
        return data[miss_cols].isnull().sum()

    if len(miss_freq(raw_data)) == 0:
        return raw_data

    def impute(data,column, value):
        data.loc[data[column].isnull(),column] = value

    ###################################
    # IMPUTATION
    ###################################
    # Treating CONTINIOUS ONLY THREE CTS variables have missing values and only GarageYrBlt is not treatable..
    for col in cts_cols:
        impute(raw_data,col,0)

    # Treating CATEGORICAL all nan become 'NA', as will have ONE-HOT-ENCODING
    for col in cat_cols:
        impute(raw_data,col,'NA')

    assert len(miss_freq(raw_data)) == 0
    return raw_data

########################################################################
# Log-transform skewed continuous feats
########################################################################
def log_transform_skewed(data, cts_cols, tol):
    #log transform skewed numeric feats:
    skewed_cols = data[cts_cols].apply(lambda col: pd.DataFrame.skew(col.dropna()))
    skewed_cols = skewed_cols[abs(skewed_cols) > tol].index
    print('Skewed feats:\n',skewed_cols)
    data[skewed_cols] = np.log1p(data[skewed_cols])
    return data

########################################################################
# Prepare data
########################################################################
def preprocess_feats(train,test):
    TARGET_COLUMN = 'SalePrice'

     #### MANUALLY SPECIFY TYPES OF COLUMNS
    CONTINUOUS_COLUMNS = ["LotFrontage","LotArea","YearBuilt","YearRemodAdd","MasVnrArea",
        "BsmtFinSF1","BsmtFinSF2","BsmtUnfSF","TotalBsmtSF",
        "1stFlrSF","2ndFlrSF","LowQualFinSF","GrLivArea","BsmtFullBath","BsmtHalfBath","FullBath","HalfBath","BedroomAbvGr","KitchenAbvGr",
        "TotRmsAbvGrd","Fireplaces","GarageYrBlt","GarageCars",
        "GarageArea","WoodDeckSF","OpenPorchSF","EnclosedPorch","3SsnPorch","ScreenPorch","PoolArea",
        "MiscVal","MoSold","YrSold"]
    CATEGORICAL_COLUMNS = ["MSSubClass","MSZoning","Street","Alley","LotShape","LandContour","Utilities","LotConfig","LandSlope","Neighborhood",
        "Condition1","Condition2","BldgType","HouseStyle","OverallQual","OverallCond","RoofStyle","RoofMatl","Exterior1st","Exterior2nd","MasVnrType",
        "ExterQual","ExterCond","Foundation","KitchenQual","BsmtQual","BsmtCond","BsmtExposure","BsmtFinType1","BsmtFinType2","HeatingQC","Functional","FireplaceQu",
        "Heating","CentralAir","Electrical","GarageType","GarageCond","PavedDrive","Fence","MiscFeature","PoolQC","SaleType","SaleCondition",
        "GarageFinish","GarageQual"]

    print("Continuous columns {}".format(len(CONTINUOUS_COLUMNS)))
    print("Categorical columns {}".format(len(CATEGORICAL_COLUMNS)))


    print("Treating missing values")
    test = treat_missing_values(test,CONTINUOUS_COLUMNS,CATEGORICAL_COLUMNS)
    train = treat_missing_values(train,CONTINUOUS_COLUMNS,CATEGORICAL_COLUMNS)



    # CATEGORICAL
    print("Encoding categorical feats")
    for col in CATEGORICAL_COLUMNS:
        print(col)
        le = preprocessing.LabelEncoder()
        le.fit(pd.concat([train[col],test[col]],ignore_index = 1))
        train[col] = le.transform(train[col])
        test[col] = le.transform(test[col])

    enc = preprocessing.OneHotEncoder(sparse=False)
    enc.fit(pd.concat([train[CATEGORICAL_COLUMNS],test[CATEGORICAL_COLUMNS]],ignore_index = 1))

# binary encoding
    train_cat = enc.transform(train[CATEGORICAL_COLUMNS])
    test_cat  = enc.transform(test[CATEGORICAL_COLUMNS])
# extracting continuous
    train_cts = train[CONTINUOUS_COLUMNS].as_matrix()
    test_cts  = test[CONTINUOUS_COLUMNS].as_matrix()
# extracting Label
    labels    = train[TARGET_COLUMN]

    ######
    # getting final data
    train_data = np.concatenate([train_cat,train_cts],1)
    test_data = np.concatenate([test_cat,test_cts],1)

    feats = get_feats(train,test)



    # Cross Features
    poly = preprocessing.PolynomialFeatures(degree=2,interaction_only=True)
    train_data2 = poly.fit_transform(train_data)
    test_data2  = poly.fit_transform(test_data)


    # train_data2 = []
    # test_data2  = []


    return train,test,feats,train_data,test_data, train_data2, test_data2, labels
