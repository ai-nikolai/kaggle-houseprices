import numpy as np
import pandas as pd
import time
import itertools
from sklearn.model_selection import train_test_split, KFold
import xgboost as xgb
from sklearn.neural_network import MLPRegressor

def get_score(y_hat,y):
    return np.sqrt(np.inner(y-y_hat,y-y_hat)/len(y))



def execute_model(train,train_data,test_data,labels):
    folds = 5
    kf = KFold(n_splits=folds, shuffle=True)
    temp_scores = np.zeros(folds)
    scores = [[0]]
    ##
    model = MLPRegressor(hidden_layer_sizes=(2000,10000),activation='tanh', learning_rate='adaptive',)    # timing
    t0 = time.time()
    i = 0
    for train_index, valid_index in kf.split(train):
        # getting the data
        X_train, X_valid  = train_data[train_index], train_data[valid_index]
        Y_train, Y_valid  = np.log(labels[train_index]), np.log(labels[valid_index])
        model.fit(X_train,Y_train)
        temp = model.predict(X_valid)
        temp_scores[i] = get_score(temp,Y_valid)
        i += 1
    #score
    score = np.mean(temp_scores)


    # Test Prediction
    model.fit(train_data,np.log(labels))
    fin_pred = model.predict(test_data)


    print('Best score: {:.6g}'.format(score))
    print('Training time: {} min'.format(round((time.time() - t0)/60, 3)))

    # # Save model:
    # bst.save_model('latest.model')
    # bst.dump_model('latestdump.raw.txt','latestfeatmap.txt')

    # # Loading a model #have not tried
    # bst = xgb.Booster({'nthread':4}) #init model
    # bst.load_model("latestmodel.bin") # load data
    return fin_pred, score, scores
