import numpy as np
import pandas as pd
import time
import itertools
from sklearn.model_selection import train_test_split, KFold
import xgboost as xgb
from sklearn import linear_model

def get_score(y_hat,y):
    return np.sqrt(np.inner(y-y_hat,y-y_hat)/len(y))


def data_feature_mapping(train,test):
    tr = np.concatenate( [train, np.power(train,2),np.power(train,3)] ,1)
    ts = np.concatenate( [test, np.power(test,2) ,np.power(test,3)]   ,1)
    return tr,ts



def execute_model(train,test,feats,target = 'SalePrice'):
    #getting the data
    train_data,test_data = data_feature_mapping(train[feats].as_matrix(),test[feats].as_matrix())
    # train_data = train[feats].as_matrix()
    # test_data  = test[feats].as_matrix()
    labels = train[target]

    # parameters for sklearn
    n1 = 5
    n2 = 10
    # l1_ratio = np.linspace(0,1,n1) #for elastic net
    l1_ratio = [0]
    alphas = np.linspace(0.001,0.01,n2)
    folds = 8

    # init
    n1 = len(l1_ratio)
    n2 = len(alphas)
    scores = np.zeros([n1,n2])
    temp_scores = np.zeros(folds)
    kf = KFold(n_splits=folds, shuffle=True)

    # timing
    t0 = time.time()

    # model = linear_model.SGDRegressor(
    #                                 loss='squared_loss',
    #                                 penalty='elasticnet',
    #                                 l1_ratio=l1_ratio[0],
    #                                 alpha=alphas[0],
    #                                 # eta=0.1
    #                                 )


    # model = linear_model.ElasticNet(l1_ratio=l1_ratio[0],alpha=alphas[0])

    model = linear_model.Ridge(alpha=alphas[0])

    #  Training and Cross Validation
    for idx in range(n1): #l1_ratio
        for jdx in range(n2): #alphas
            # model.set_params(l1_ratio=l1_ratio[idx],alpha=alphas[jdx])
            model.set_params(alpha=alphas[jdx])

            # model.set_params(
            #                                 loss='squared_loss',
            #                                 penalty='elasticnet',
            #                                 l1_ratio=l1_ratio[idx],
            #                                 alpha=alphas[jdx],
            #                                 # eta=0.1
            #                                 )
            # Cross Validation
            i = 0
            for train_index, valid_index in kf.split(train):
                # getting the data
                X_train, X_valid  = train_data[train_index], train_data[valid_index]
                Y_train, Y_valid  = np.log(labels[train_index]), np.log(labels[valid_index])
                model.fit(X_train,Y_train)
                temp = model.predict(X_valid)
                temp_scores[i] = get_score(temp,Y_valid)
                i = i + 1

            # computing the average score for this method
            scores[idx,jdx] = np.mean(temp_scores)
            print('Current Params: alpha=%f, l1_ratio=%f; SCORE: %f'%(alphas[jdx],l1_ratio[idx],scores[idx,jdx]))

    # model = linear_model.LinearRegression()




    ###################################
    #



    # Parameters
    params = model.get_params(False)

    # Test Prediction
    fin_pred = model.predict(test_data)

    #mse
    score  = np.min(scores)


    print('Training time: {} min'.format(round((time.time() - t0)/60, 3)))

    # # Save model:
    # bst.save_model('latest.model')
    # bst.dump_model('latestdump.raw.txt','latestfeatmap.txt')

    # # Loading a model #have not tried
    # bst = xgb.Booster({'nthread':4}) #init model
    # bst.load_model("latestmodel.bin") # load data
    return fin_pred, score, scores
