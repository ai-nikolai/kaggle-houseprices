import numpy as np
import pandas as pd
import time
import itertools
from sklearn.model_selection import train_test_split, KFold
import xgboost as xgb
from sklearn import linear_model

def get_score(y_hat,y):
    return np.sqrt(np.inner(y-y_hat,y-y_hat)/len(y))


def data_feature_mapping(train,test):
    tr = np.concatenate(( train, np.power(train,2),np.power(train,3) ),1)
    ts = np.concatenate((  test, np.power(test,2) ,np.power(test,3)  ),1)
    return tr,ts



def execute_model(train,test,feats,target = 'SalePrice'):
    #getting the data
    X_train,test_data = data_feature_mapping(train[feats].as_matrix(),test[feats].as_matrix())
    Y_train = np.log(train[target].as_matrix())

    # parameters for sklearn
    n1 = 5
    n2 = 20
    l1_ratio = np.linspace(0,1,n1) #for elastic net
    alphas = np.linspace(0.01,2,n2)
    folds = 4

    # init
    scores = np.zeros([n1,n2])
    temp_scores = np.zeros(folds)
    kf = KFold(n_splits=folds, shuffle=True)

    # timing
    t0 = time.time()

    # model = linear_model.OrthogonalMatchingPursuit()
    model = linear_model.LinearRegression()
    model.fit(X_train,Y_train)





    ###################################
    #

    # Parameters
    params = model.get_params(False)

    # Test Prediction
    fin_pred = model.predict(test_data)
    temp = model.predict(X_train)
    #mse
    score  = get_score(temp,Y_train)


    print('Training time: {} min'.format(round((time.time() - t0)/60, 3)))

    # # Save model:
    # bst.save_model('latest.model')
    # bst.dump_model('latestdump.raw.txt','latestfeatmap.txt')

    # # Loading a model #have not tried
    # bst = xgb.Booster({'nthread':4}) #init model
    # bst.load_model("latestmodel.bin") # load data
    return fin_pred, score, scores
