import numpy as np
import pandas as pd
import time
import itertools
from sklearn.model_selection import train_test_split, KFold
import xgboost as xgb
from sklearn import linear_model
from sklearn import kernel_ridge

def get_score(y_hat,y):
    return np.sqrt(np.inner(y-y_hat,y-y_hat)/len(y))



def execute_model(train,train_data,test_data,labels):
    # parameters for sklearn
    # parameters for sklearn
    n1 = 5
    n2 = 5
    # l1_ratio = np.linspace(0,1,n1) #for elastic net
    gammas  = np.exp(np.linspace(-5,1,n1))
    alphas  = np.exp(np.linspace(-4,0,n2))
    folds = 3

    # init
    n1 = len(gammas)
    n2 = len(alphas)
    scores = np.zeros([n1, n2])
    temp_scores = np.zeros(folds)
    kernels = ['poly','rbf','sigmoid','chi2']


    kf = KFold(n_splits=folds, shuffle=True)

    # timing
    t0 = time.time()


    # model = linear_model.Lasso(alpha=alphas[0])
    # model = linear_model.ElasticNet(l1_ratio=l1_ratios[0],alpha=alphas[0])
    model = kernel_ridge.KernelRidge(gamma=gammas[0],alpha=alphas[0],kernel=kernels[2],degree=2)

    #  Training and Cross Validation
    for idx in range(n1): #gammas
        for jdx in range(n2): #alphas
            model.set_params(gamma=gammas[idx],alpha=alphas[jdx],kernel=kernels[2],degree=2)
            i = 0
            for train_index, valid_index in kf.split(train):
                # getting the data
                X_train, X_valid  = train_data[train_index], train_data[valid_index]
                Y_train, Y_valid  = np.log(labels[train_index]), np.log(labels[valid_index])
                model.fit(X_train,Y_train)
                temp = model.predict(X_valid)
                temp_scores[i] = get_score(temp,Y_valid)
                i = i + 1

            # computing the average score for this method
            scores[idx,jdx] = np.mean(temp_scores)
            print('Current  SCORE: %f'%(scores[idx,jdx]))
            print('Current Params: Gamma=%f, Alpha=%f'%(gammas[idx],alphas[jdx]))
    # model = linear_model.LinearRegression()




    ###################################
    #
    gdx,adx = np.where(scores==scores.min())[0][0], np.where(scores==scores.min())[1][0]
    alpha_best = alphas[adx]
    gamma_best    = gammas[gdx]

    #train again
    model.set_params(gamma=gamma_best,alpha=alpha_best,kernel=kernels[2],degree=2)
    model.fit(train_data,np.log(labels))

    print('BEST PARAMS: Gamma=%f, Alpha=%f'%(gamma_best,alpha_best))
    # Parameters
    params = model.get_params(False)

    # Test Prediction
    fin_pred = model.predict(test_data)

    #mse
    score  = np.min(scores)

    print('Best score: {:.6g}'.format(score))
    print('Training time: {} min'.format(round((time.time() - t0)/60, 3)))

    # # Save model:
    # bst.save_model('latest.model')
    # bst.dump_model('latestdump.raw.txt','latestfeatmap.txt')

    # # Loading a model #have not tried
    # bst = xgb.Booster({'nthread':4}) #init model
    # bst.load_model("latestmodel.bin") # load data
    return fin_pred, score, scores
