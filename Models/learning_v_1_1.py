import numpy as np
import pandas as pd
import time
import itertools
from sklearn.model_selection import train_test_split, KFold
import xgboost as xgb


def execute_model(train,train_data,test_data,labels):
    folds = 2
    random_state = 1

    eta_list = [0.03] # list of parameters to try
    lambdas  = np.exp([-5,-4,-3,-2,-1,0])#np.linspace(1,2,4)
    # alphas   = [0,1e-3,1e-2,1e-1,1]
    max_depth = 4#[2,3,4,5,6] # list of parameters to try


    # eta_list = [0.01] # list of parameters to try
    # max_depth_list = [4] # list of parameters to try

    num_boost_round = 7000 # should be improportional to eta
    early_stopping_rounds = 700
    subsample = 1 # like dropout, recommend no subsampling ( = 1) as we have cross-validation and litle data
    colsample_bytree = 1

    t0 = time.time()

    # start the training
    scores = np.zeros([len(eta_list),len(lambdas)]) # store score values
    temp_scores = np.zeros(folds)


    kf = KFold(n_splits=folds, random_state=random_state, shuffle=True)
    for idx in range(len(eta_list)):# Loop over parameters to find the better set
        for jdx in range(len(lambdas)):
            print('XGBoost params. Eta: {}, LAMBDAS: {}'.format(eta_list[idx], lambdas[jdx]))
            params = {
                "objective": "reg:linear",
                # "booster" : "gbtree", #gblinear, dart
                # "booster" : "gblinear",
                "booster" : "dart",
                "eval_metric": "rmse", # as defined in kaggle
                "eta": eta_list[0], # lower parameters tend to prevent overfitting
                "tree_method": 'exact',
                "max_depth": max_depth,
                "subsample": subsample,
                "colsample_bytree": colsample_bytree,
                "silent": 1,
                "seed": 0,
                "lambda": lambdas[jdx],
                # "alpha": alphas[idx]
            }
            i = 0
            for train_index, valid_index in kf.split(train_data):
                X_train, X_valid  = train_data[train_index], train_data[valid_index]
                Y_train, Y_valid  = np.log(labels[train_index]), np.log(labels[valid_index])

                # Dmatrices for xboost compatibility:
                dtrain = xgb.DMatrix(X_train, Y_train)
                dvalid = xgb.DMatrix(X_valid, Y_valid)
                # evaluate and print according to watchlist
                watchlist = [(dtrain, 'train'), (dvalid, 'eval')]
                # TRAIN:
                bst = xgb.train(params, dtrain, num_boost_round, evals=watchlist, early_stopping_rounds=early_stopping_rounds, verbose_eval=False) # find the best score


                # score
                temp_scores[i] = bst.best_score
                i = i+1

            scores[idx,jdx] = np.average(temp_scores)
            print('Current Score%f'%(scores[idx,jdx]))


    adx,dx = np.where(scores==scores.min())[0][0], np.where(scores==scores.min())[1][0]


    params['eta'], params['lambda'] = eta_list[adx], lambdas[dx]
    # train again
    dtrain = xgb.DMatrix(train_data, np.log(labels))
    bst = xgb.train(params, dtrain, num_boost_round, evals= [(dtrain, 'train')], early_stopping_rounds=early_stopping_rounds, verbose_eval=False)

    # train results
    score = scores.min()
    print('Best score: {:.6g}'.format(score))

    print('BEST PARAMS: Eta=%f, LAMBDA=%d'%(eta_list[adx],lambdas[dx]))

    # predict on test
    fin_pred = bst.predict(xgb.DMatrix(test_data), ntree_limit=bst.best_ntree_limit)


    # bst.load_model("latestmodel.bin") # load data
    return fin_pred, score, scores







        # importance = sorted(bst.get_fscore().items(), key=itemgetter(1), reverse=1) # importance := bst.get_fscore()
        #     print('Features importance :\n{}'.format(importance))
        #     print('Number of features:', len(feats))
        #     print('Features\n',feats)
