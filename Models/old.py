import numpy as np
import pandas as pd
import csv,datetime
from sklearn.model_selection import train_test_split, KFold
import xgboost as xgb
from operator import itemgetter
import time
import matplotlib.pyplot as plt
import itertools
from sklearn import preprocessing

#TODO:
# removing irrelevant feats (L1 ridge regression -> removing feats with abs(weight)<tolerance )
# cross feats
# even more transformations (based on detailed data analysis)
# hyperparameter optimization/gridSearch

#
#
#

# If above done:
# changing treatment of missing values e.g. substitute 'NA' values differently than by 'mean'/typical values
# (possibly trying out different models)

########################################################################
# Save prediction to format desired by Kaggle:
########################################################################

def save_predictions(predictions,test,score):

    filename = 'submission_%f_%s.csv'%(score,
        datetime.datetime.now().strftime("%Y-%m-%d-%H-%M"))
    prediction_writer = csv.writer( open(filename, 'w', newline='') )
    prediction_writer.writerow(["Id","SalePrice"]) # Header
    for i in range(len(test)):
        prediction_writer.writerow([test.Id[i], predictions[i]])
    print('Submission created: ', filename)


########################################################################
# Extract relevant feats that are both in train and test data
########################################################################
def get_feats(train, test):
    train_cols = set(train.columns.values)
    train_cols.remove('SalePrice')
    test_cols = set(test.columns.values)
    feats = list(train_cols & test_cols)
    feats.remove('Id')
    return feats

########################################################################
# Deal with missing values
########################################################################
def treat_missing_values(raw_data,cts_cols,cat_cols):

    def miss_freq(data):
        # returns frequency table of missing words for each column (0 freq. not listed)
        miss_cols = data.columns[data.isnull().any()].tolist() # cols with missing values
        return data[miss_cols].isnull().sum()

    if len(miss_freq(raw_data)) == 0:
        return raw_data

    def impute(data,column, value):
        # Impute missing value
        data.loc[data[column].isnull(),column] = value

    # COLUMNS TO TREAT MISSING VALUESS individually:
    bsmt_cols_2 = ['BsmtQual','BsmtCond','BsmtExposure','BsmtFinType1','BsmtFinType2']
    garage_cols = ['GarageType','GarageQual','GarageCond','GarageYrBlt','GarageFinish','GarageCars','GarageArea']
    other_cols = ['LotArea', 'LotFrontage', 'Alley', 'MasVnrArea',
                    'MasVnrType', 'Electrical', 'FireplaceQu', 'PoolQC', 'Fence', 'MiscFeature']

    raw_data = raw_data.assign(logLotArea = lambda x: np.log(x['LotArea']+1e2))
    ind = raw_data['LotFrontage'].isnull()
    raw_data.loc[ind,'LotFrontage'] = raw_data.loc[ind,'logLotArea']
    del raw_data['logLotArea']

    impute(raw_data,'Alley','NA')
    impute(raw_data,'MasVnrArea', 0.)
    impute(raw_data,'MasVnrType','None')
    for col in bsmt_cols_2:
        impute(raw_data,col,'NA')
    impute(raw_data,'Electrical','SBrkr')
    impute(raw_data,'FireplaceQu','NA')
    for col in garage_cols:
        if raw_data[col].dtype == np.object:
            impute(raw_data,col,'NA')
        else:
            impute(raw_data,col, 0)
    impute(raw_data,'PoolQC', 'NA')
    impute(raw_data,'Fence', 'NA')
    impute(raw_data,'MiscFeature', 'NA')

    # Handle the remaining columns
    remaining_cols = list(set(cts_cols + cat_cols) - set(bsmt_cols_2 + garage_cols + other_cols))
    for col in remaining_cols:
        if raw_data[col].dtype ==  'O':
            raw_data[col].fillna('NA', inplace=1)
        else:
            raw_data[col].fillna(0, inplace=1)

    ## CONVERT CTS -> CAT and CAT -> CTS based on common-sense
    #  Note that 'NA' are assumed to be mean values
    raw_data = raw_data.replace({
                            'MSSubClass':{
                                            20:'c20',
                                            30:'c30',
                                            40:'c40',
                                            45:'c45',
                                            50:'c50',
                                            60:'c60',
                                            70:'c70',
                                            75:'c75',
                                            80:'c80',
                                            85:'c85',
                                            90:'c90',
                                            120:'c120',
                                            150:'c150',
                                            160:'c160',
                                            180:'c180',
                                            190:'c190'
                                            },
                            'MSZoning': {'C (all)': 'C'
                                            },
                            'ExterQual': {'Ex': 5,
                                            'Gd': 4,
                                            'TA': 3,
                                            'Fa': 2,
                                            'Po': 1
                                            },
                            'ExterCond': {'Ex': 5,
                                            'Gd': 4,
                                            'TA': 3,
                                            'Fa': 2,
                                            'Po': 1
                                            },
                            'BsmtQual':{
                                            'Ex':5,
                                            'Gd':4,
                                            'TA':3,
                                            'Fa':2,
                                            'Po':1,
                                            'NA': 3},
                            'BsmtCond': {'Ex': 5,
                                            'Gd': 4,
                                            'TA': 3,
                                            'Fa': 2,
                                            'Po': 1,
                                            'NA': 3},
                            'BsmtExposure': {'Gd':4,
                                            'Av':3,
                                            'Mn':2,
                                            'No':1,
                                            'NA':3},
                            'BsmtFinType1':{'GLQ':6,
                                            'ALQ':5,
                                            'BLQ':4,
                                            'Rec':3,
                                            'LwQ':2,
                                            'Unf':1,
                                            'NA':0},
                            'BsmtFinType2':{
                                            'GLQ':6,
                                            'ALQ':5,
                                            'BLQ':4,
                                            'Rec':3,
                                            'LwQ':2,
                                            'Unf':1,
                                            'NA':0},
                            'HeatingQC': {'Ex': 5,
                                            'Gd': 4,
                                            'TA': 3,
                                            'Fa': 2,
                                            'Po': 1
                                            },
                            'KitchenQual': {'Ex': 5,
                                            'Gd': 4,
                                            'TA': 3,
                                            'Fa': 2,
                                            'Po': 1,
                                            'NA':3},
                            'Functional': {'Typ': 8,
                                            'Min1': 7,
                                            'Min2': 6,
                                            'Mod': 5,
                                            'Maj1': 4,
                                            'Maj2': 3,
                                            'Sev': 2,
                                            'Sal': 1,
                                            'NA': 8},
                            'FireplaceQu': {'Ex': 5,
                                            'Gd': 4,
                                            'TA': 3,
                                            'Fa': 2,
                                            'Po': 1,
                                            'NA': 3
                                            },
                            'GarageFinish': {
                                            'Fin':3,
                                            'RFn':2,
                                            'Unf':1,
                                            'NA':0
                                             },
                            'GarageQual': {'Ex': 5,
                                            'Gd': 4,
                                            'TA': 3,
                                            'Fa': 2,
                                            'Po': 1,
                                            'NA': 3},
                            'GarageCond': {'Ex': 5,
                                            'Gd': 4,
                                            'TA': 3,
                                            'Fa': 2,
                                            'Po': 1,
                                            'NA': 3},
                            'PoolQC': {'Ex':4,
                                            'Gd':3,
                                            'TA':2,
                                            'Fa':1,
                                            'NA':2
                                       }
                            })

    assert len(miss_freq(raw_data)) == 0

    return raw_data

########################################################################
# Log-transform skewed continuous feats
########################################################################
def log_transform_skewed(data, cts_cols, tol):
    #log transform skewed numeric feats:
    skewed_cols = data[cts_cols].apply(lambda col: pd.DataFrame.skew(col.dropna()))
    skewed_cols = skewed_cols[abs(skewed_cols) > tol].index
    print('Skewed feats:\n',skewed_cols)
    data[skewed_cols] = np.log1p(data[skewed_cols])
    return data

########################################################################
# Prepare data
########################################################################
def preprocess_feats(train,test):

     #### MANUALLY SPECIFY TYPES OF COLUMNS
    CONTINUOUS_COLUMNS = ["LotFrontage","LotArea","OverallQual","OverallCond","YearBuilt","YearRemodAdd","MasVnrArea","ExterQual","ExterCond",
        "BsmtQual","BsmtCond","BsmtExposure","BsmtFinType1","BsmtFinSF1","BsmtFinType2","BsmtFinSF2","BsmtUnfSF","TotalBsmtSF","HeatingQC",
        "1stFlrSF","2ndFlrSF","LowQualFinSF","GrLivArea","BsmtFullBath","BsmtHalfBath","FullBath","HalfBath","BedroomAbvGr","KitchenAbvGr",
        "KitchenQual","TotRmsAbvGrd","Functional","Fireplaces","FireplaceQu","GarageYrBlt","GarageFinish","GarageCars",
        "GarageArea","GarageQual","GarageCond","WoodDeckSF","OpenPorchSF","EnclosedPorch","3SsnPorch","ScreenPorch","PoolArea","PoolQC",
        "MiscVal","MoSold","YrSold"]
    CATEGORICAL_COLUMNS = ["MSSubClass","MSZoning","Street","Alley","LotShape","LandContour","Utilities","LotConfig","LandSlope","Neighborhood",
        "Condition1","Condition2","BldgType","HouseStyle","RoofStyle","RoofMatl","Exterior1st","Exterior2nd","MasVnrType",
        "Foundation","Heating","CentralAir","Electrical","GarageType","PavedDrive","Fence","MiscFeature","SaleType","SaleCondition"]

    print("Continuous columns {}".format(len(CONTINUOUS_COLUMNS)))
    print("Categorical columns {}".format(len(CATEGORICAL_COLUMNS)))

    print("Treating missing values")
    test = treat_missing_values(test,CONTINUOUS_COLUMNS,CATEGORICAL_COLUMNS)
    train = treat_missing_values(train,CONTINUOUS_COLUMNS,CATEGORICAL_COLUMNS)

    print("Transforming skewed continuous feats - SKEW")
    skew_tol = 1.96 #1.
    test = log_transform_skewed(test, CONTINUOUS_COLUMNS, skew_tol)
    train = log_transform_skewed(train, CONTINUOUS_COLUMNS, skew_tol)

    print("Creating cross feats")
    #TODO

    # categorical_feats = train.select_dtypes(include = ["object"]).columns.values
    print("Encoding categorical feats")
    for col in CATEGORICAL_COLUMNS:
        print(col)
        le = preprocessing.LabelEncoder()
        le.fit(pd.concat([train[col],test[col]],ignore_index = 1))
        train[col] = le.transform(train[col])
        test[col] = le.transform(test[col])

    assert (len(train.select_dtypes(include = ["object"]).columns.values) == 0 &
                len(test.select_dtypes(include = ["object"]).columns.values) == 0)

    print("Extracting final feats")
    feats = get_feats(train,test)

    return train,test,feats


def train_n_test_Kfold(train,test,feats,target = 'SalePrice'):
    n_splits = 8
    random_state = 1
    eta_list = [0.025] #[0.005,0.01,0.015,0.02,0.025] # list of parameters to try
    max_depth_list = [2] #[2,3,4,5,6] # list of parameters to try
    num_boost_round = 7000 # should be improportional to eta
    early_stopping_rounds = 700
    subsample = 1 # like dropout, recommend no subsampling ( = 1) as we have cross-validation and litle data
    colsample_bytree = 1

    predict_test_by_average_kfold = 1#0
    t0 = time.time()

    # start the training
    scores = np.ndarray((len(eta_list)*len(max_depth_list),4)) # store score values
    i = 0
    for eta,max_depth in list(itertools.product(eta_list, max_depth_list)): # Loop over parameters to find the better set
        print('XGBoost params. ETA: {}, MAX_DEPTH: {}'.format(eta, max_depth))
        params = {
            "objective": "reg:linear",
            "booster" : "gbtree",
            "eval_metric": "rmse", # as defined in kaggle
            "eta": eta, # lower parameters tend to prevent overfitting
            "tree_method": 'exact',
            "max_depth": max_depth,
            "subsample": subsample,
            "colsample_bytree": colsample_bytree,
            "silent": 1,
            "seed": 0,
        }
        kf = KFold(n_splits=n_splits, random_state=random_state, shuffle=True)
        test_pred = np.ndarray((n_splits,len(test)))
        fold = 0
        fold_score = []
        for train_index, valid_index in kf.split(train):
            X_train, X_valid  = train[feats].as_matrix()[train_index], train[feats].as_matrix()[valid_index]
            y_train, y_valid  = np.log(train[target].as_matrix()[train_index]), np.log(train[target].as_matrix()[valid_index])

            # Dmatrices for xboost compatibility:
            dtrain = xgb.DMatrix(X_train, y_train)
            dvalid = xgb.DMatrix(X_valid, y_valid)

            # evaluate and print according to watchlist
            watchlist = [(dtrain, 'train'), (dvalid, 'eval')]

            # TRAIN:
            bst = xgb.train(params, dtrain, num_boost_round, evals=watchlist, early_stopping_rounds=early_stopping_rounds, verbose_eval=True) # find the best score

            # check_valid = bst.predict(xgb.DMatrix(X_valid),ntree_limit=bst.best_ntree_limit) # get the best score??

            score = bst.best_score
            print('Best score: {:.6f}'.format(score))
            fold_score.append(score)

            print("Predict validation:")
            pred = bst.predict(xgb.DMatrix(test[feats].as_matrix()),ntree_limit=bst.best_ntree_limit)
            #np.save("Feature importance",importance)
            #np.save("Predictions_eta%s_depth%s_fold%s" %(eta,max_depth,fold),prediction)
            test_pred[fold] = pred
            fold += 1

        xgb.plot_importance(bst)
        mean_score = np.mean(fold_score)
        print("Mean Score : {}, eta : {}, depth : {}\n".format(mean_score,eta,max_depth))
        scores[i] = [eta, max_depth, mean_score, np.std(fold_score)]
        i += 1

    # Test prediction
    if predict_test_by_average_kfold:
        fin_pred = test_pred.mean(axis=0)  # final prediction obtained by averaging over fold test predictions
    else:
        # take best params by argmin-ing mean_score
        best_row = np.argmin(scores[:,2])
        # update params to best params
        params['eta'], params['max_depth'] = scores[best_row,0], int(scores[best_row,1])
        # train again
        dtrain = xgb.DMatrix(train[feats].as_matrix(), np.log(train[target].as_matrix()))
        bst = xgb.train(params, dtrain, num_boost_round, evals= [(dtrain, 'train')], early_stopping_rounds=early_stopping_rounds, verbose_eval=True)
        # train results
        score = bst.best_score
        print('Best score: {:.6g}'.format(score))
        fold_score.append(score)
        importance = sorted(bst.get_fscore().items(), key=itemgetter(1), reverse=1) # importance := bst.get_fscore()
        print('Features importance :\n{}'.format(importance))
        print('Number of features:', len(feats))
        print('Features\n',feats)
        # predict on test
        fin_pred = bst.predict(xgb.DMatrix(test[feats].as_matrix()), ntree_limit=bst.best_ntree_limit)


    all_score_table = pd.DataFrame(scores,columns=['eta','max_depth','mean_score','std_score'])
    print ("All scores: \n", all_score_table) # choose the best model wrt params
    print('Best parameters:', params)
    print('Training time: {} min'.format(round((time.time() - t0)/60, 3)))

    # # Save model:
    # bst.save_model('latest.model')
    # bst.dump_model('latestdump.raw.txt','latestfeatmap.txt')

    # # Loading a model #have not tried
    # bst = xgb.Booster({'nthread':4}) #init model
    # bst.load_model("latestmodel.bin") # load data
    return fin_pred, mean_score

############################################################################
# Main code
###########################################################################
if __name__ ==  "__main__":

    train = pd.read_csv("../train.csv")
    test = pd.read_csv("../test.csv")

    train,test,feats = preprocess_feats(train,test)

    test_pred, score = train_n_test_Kfold(train,test,feats)

    save_predictions(np.exp(test_pred),test,score)



    #test_pred,score = train_and_test_linear(train,test,feats)
    #test_pred,score = train_and_test_tree(train,test,feats) # run at least once this one to get the feats importance
    #feats = np.load("feats_importance.npy")

# def train_and_test_linear(train,test,feats,target = 'SalePrice'): # simple xgboost
#     subsample = 0.8
#     colsample_bytree = 0.8
#     num_boost_round = 1000 #115 originally
#     early_stopping_rounds = 50
#     test_size = 0.2 # 0.1 originally

#     start_time = time.time()

#     # start the training

#     params = {
#         "objective": "reg:linear",
#         "booster" : "gblinear", #"gbtree",# default
#         "eval_metric": "rmse",
#         "subsample": subsample, # collect 80% of the data only to prevent overfitting
#         "colsample_bytree": colsample_bytree,
#         "silent": 1,
#         "seed": 0,
#     }

#     X_train, X_valid = train_test_split(train, test_size = test_size, random_state = 0) # randomly split into 90% test and 10% CV -> still has the outcome at this point
#     y_train = np.log(X_train[target]) # define y as the outcome column, apply log to have same error as the leaderboard
#     y_valid = np.log(X_valid[target])
#     dtrain = xgb.DMatrix(X_train[feats], y_train) # DMatrix are matrix for xgboost
#     dvalid = xgb.DMatrix(X_valid[feats], y_valid)

#     watchlist = [(dtrain, 'train'), (dvalid, 'eval')] # list of things to evaluate and print
#     bst = xgb.train(params, dtrain, num_boost_round, evals = watchlist, early_stopping_rounds = early_stopping_rounds, verbose_eval = True) # find the best score
#     score = bst.best_score #roc_auc_score(X_valid[target].values, check)
#     print('Last error value: {:.6f}'.format(score))

#     print("Predict test set...")
#     test_pred = bst.predict(xgb.DMatrix(test[feats]))

#     print('Training time: {} minutes'.format(round((time.time() - start_time)/60, 2)))

#     return test_pred, score

# def train_and_test_tree(train,test,feats,target = 'SalePrice'): # simple xgboost
#     eta_list = [0.1,0.2] # list of parameters to try
#     max_depth_list = [4,6,8] # list of parameters to try
#     subsample = 0.8
#     colsample_bytree = 0.8

#     num_boost_round = 400
#     early_stopping_rounds = 10
#     test_size = 0.2

#     start_time = time.time()

#     # start the training
#     array_score = np.ndarray((len(eta_list)*len(max_depth_list),3)) # store score values
#     i = 0
#     for eta,max_depth in list(itertools.product(eta_list, max_depth_list)): # Loop over parameters to find the better set
#         print('XGBoost params. ETA: {}, MAX_DEPTH: {}, SUBSAMPLE: {}, COLSAMPLE_BY_TREE: {}'.format(eta, max_depth, subsample, colsample_bytree))
#         params = {
#             "objective": "reg:linear",
#             "booster" : "gbtree",
#             "eval_metric": "rmse", # as given by kaggle
#             "eta": eta, # shrink against overfitting
#             "tree_method": 'exact',
#             "max_depth": max_depth,
#             "subsample": subsample, # drop-out
#             "colsample_bytree": colsample_bytree,
#             "silent": 1,
#             "seed": 0,
#         }

#         X_train, X_valid = train_test_split(train, test_size = test_size, random_state = 0) # randomly split into 90% test and 10% CV -> still has the outcome at this point
#         y_train = np.log(X_train[target]) # define y as the outcome column
#         y_valid = np.log(X_valid[target])
#         dtrain = xgb.DMatrix(X_train[feats], y_train) # DMatrix are matrix for xgboost
#         dvalid = xgb.DMatrix(X_valid[feats], y_valid)

#         watchlist = [(dtrain, 'train'), (dvalid, 'eval')] # list of things to evaluate and print
#         bst = xgb.train(params, dtrain, num_boost_round, evals = watchlist, early_stopping_rounds = early_stopping_rounds, verbose_eval = True) # find the best score

#         print("Validating...")
#         score = bst.best_score
#         print('Last error value: {:.6f}'.format(score))
#         array_score[i][0] = eta
#         array_score[i][1] = max_depth
#         array_score[i][2] = score
#         i+ = 1
#     df_score = pd.DataFrame(array_score,columns = ['eta','max_depth','SalePrice'])
#     print("df_score : \n", df_score)
#     #create_feature_map(feats)
#     importance = bst.get_fscore()
#     importance = sorted(importance.items(), key = itemgetter(1), reverse = True)
#     print('Importance array: ', importance)
#     np.save("feats_importance",importance) # save feature importance for latter use
#     print("Predict test set...")
#     test_pred = bst.predict(xgb.DMatrix(test[feats]), ntree_limit = bst.best_ntree_limit) # only predict with the last set of parameters

#     print('Training time: {} minutes'.format(round((time.time() - start_time)/60, 2)))

#     return test_pred, score
