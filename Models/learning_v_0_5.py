import numpy as np
import pandas as pd
import time
import itertools
from sklearn.model_selection import train_test_split, KFold
import xgboost as xgb
from sklearn import svm

def get_score(y_hat,y):
    return np.sqrt(np.inner(y-y_hat,y-y_hat)/len(y))

    #####################################################################
    #  Possible Kernels
    ####################################################################
    #   ===============   ========================================
    #   metric            Function
    #   ===============   ========================================
    #   'additive_chi2'   sklearn.pairwise.additive_chi2_kernel
    #   'chi2'            sklearn.pairwise.chi2_kernel
    #   'linear'          sklearn.pairwise.linear_kernel
    #   'poly'            sklearn.pairwise.polynomial_kernel
    #   'polynomial'      sklearn.pairwise.polynomial_kernel
    #   'rbf'             sklearn.pairwise.rbf_kernel
    #   'laplacian'       sklearn.pairwise.laplacian_kernel
    #   'sigmoid'         sklearn.pairwise.sigmoid_kernel
    #   'cosine'          sklearn.pairwise.cosine_similarity
    #   ===============   ========================================



def execute_model(train,test,feats,target = 'SalePrice'):
    #getting the data
    train_data = train[feats].as_matrix()
    test_data  = test[feats].as_matrix()
    labels = train[target]



    # parameters for sklearn
    n1 = 10
    n2 = 20
    # l1_ratio = np.linspace(0,1,n1) #for elastic net
    Cs        = np.exp(np.linspace(-5,4,n1))
    epsilons  = np.linspace(0.01,0.2,n2)
    degrees = np.array([1,2,3,4,5])
    kernels = ['poly','rbf','sigmoid','chi2']
    folds = 8

    # init
    n1 = len(Cs)
    n2 = len(epsilons)
    # n3 = len(degrees)

    # scores = np.zeros([n1,n2])
    scores = np.zeros([n1,n2])
    temp_scores = np.zeros(folds)
    kf = KFold(n_splits=folds, shuffle=True)

    # timing
    t0 = time.time()

    #  Training and Cross Validation
    # for idx in range(n1): #gammas
    for idx in range(n1): #Cs
        for jdx in range(n2): #epsilons
            model = svm.SVR(C=Cs[idx],kernel=kernels[1],degree=2,epsilon=epsilons[jdx])
            # for kdx in range(n3): #degree
            # model = kernel_ridge.KernelRidge(alpha=alphas[jdx],kernel=kernels[2],degree=3)
            # Cross Validation
            i = 0
            for train_index, valid_index in kf.split(train):
                # getting the data
                X_train, X_valid  = train_data[train_index], train_data[valid_index]
                Y_train, Y_valid  = np.log(labels[train_index]), np.log(labels[valid_index])
                model.fit(X_train,Y_train)
                temp = model.predict(X_valid)
                temp_scores[i] = get_score(temp,Y_valid)
                i = i + 1

                # computing the average score for this method
            scores[idx,jdx] = np.mean(temp_scores)
            print('Current Params: alpha=%f; SCORE: %f'%(alphas[jdx],scores[idx,jdx]))








    ###################################
    #

    # Parameters
    params = model.get_params(False)

    # Test Prediction
    fin_pred = model.predict(test_data)

    #mse
    score  = np.min(scores)


    print('Training time: {} min'.format(round((time.time() - t0)/60, 3)))

    # # Save model:
    # bst.save_model('latest.model')
    # bst.dump_model('latestdump.raw.txt','latestfeatmap.txt')

    # # Loading a model #have not tried
    # bst = xgb.Booster({'nthread':4}) #init model
    # bst.load_model("latestmodel.bin") # load data
    return fin_pred, score, scores
