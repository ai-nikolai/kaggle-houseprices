import numpy as np
import pandas as pd
import time
import itertools
from sklearn.model_selection import train_test_split, KFold
import xgboost as xgb


def execute_model(train,test,feats,target = 'SalePrice'):
    n_splits = 8
    random_state = 1
    eta_list = [0.025] #[0.005,0.01,0.015,0.02,0.025] # list of parameters to try
    max_depth_list = [2] #[2,3,4,5,6] # list of parameters to try
    num_boost_round = 7000 # should be improportional to eta
    early_stopping_rounds = 700
    subsample = 1 # like dropout, recommend no subsampling ( = 1) as we have cross-validation and litle data
    colsample_bytree = 1

    predict_test_by_average_kfold = 1#0
    t0 = time.time()

    # start the training
    scores = np.ndarray((len(eta_list)*len(max_depth_list),4)) # store score values
    i = 0
    for eta,max_depth in list(itertools.product(eta_list, max_depth_list)): # Loop over parameters to find the better set
        print('XGBoost params. ETA: {}, MAX_DEPTH: {}'.format(eta, max_depth))
        params = {
            "objective": "reg:linear",
            "booster" : "gbtree",
            "eval_metric": "rmse", # as defined in kaggle
            "eta": eta, # lower parameters tend to prevent overfitting
            "tree_method": 'exact',
            "max_depth": max_depth,
            "subsample": subsample,
            "colsample_bytree": colsample_bytree,
            "silent": 1,
            "seed": 0,
        }
        kf = KFold(n_splits=n_splits, random_state=random_state, shuffle=True)
        test_pred = np.ndarray((n_splits,len(test)))
        fold = 0
        fold_score = []
        for train_index, valid_index in kf.split(train):
            X_train, X_valid  = train[feats].as_matrix()[train_index], train[feats].as_matrix()[valid_index]
            y_train, y_valid  = np.log(train[target].as_matrix()[train_index]), np.log(train[target].as_matrix()[valid_index])

            # Dmatrices for xboost compatibility:
            dtrain = xgb.DMatrix(X_train, y_train)
            dvalid = xgb.DMatrix(X_valid, y_valid)

            # evaluate and print according to watchlist
            watchlist = [(dtrain, 'train'), (dvalid, 'eval')]

            # TRAIN:
            bst = xgb.train(params, dtrain, num_boost_round, evals=watchlist, early_stopping_rounds=early_stopping_rounds, verbose_eval=True) # find the best score

            # check_valid = bst.predict(xgb.DMatrix(X_valid),ntree_limit=bst.best_ntree_limit) # get the best score??

            score = bst.best_score
            print('Best score: {:.6f}'.format(score))
            fold_score.append(score)

            print("Predict validation:")
            pred = bst.predict(xgb.DMatrix(test[feats].as_matrix()),ntree_limit=bst.best_ntree_limit)
            #np.save("Feature importance",importance)
            #np.save("Predictions_eta%s_depth%s_fold%s" %(eta,max_depth,fold),prediction)
            test_pred[fold] = pred
            fold += 1

        xgb.plot_importance(bst)
        mean_score = np.mean(fold_score)
        print("Mean Score : {}, eta : {}, depth : {}\n".format(mean_score,eta,max_depth))
        scores[i] = [eta, max_depth, mean_score, np.std(fold_score)]
        i += 1

    # Test prediction
    if predict_test_by_average_kfold:
        fin_pred = test_pred.mean(axis=0)  # final prediction obtained by averaging over fold test predictions
    else:
        # take best params by argmin-ing mean_score
        best_row = np.argmin(scores[:,2])
        # update params to best params
        params['eta'], params['max_depth'] = scores[best_row,0], int(scores[best_row,1])
        # train again
        dtrain = xgb.DMatrix(train[feats].as_matrix(), np.log(train[target].as_matrix()))
        bst = xgb.train(params, dtrain, num_boost_round, evals= [(dtrain, 'train')], early_stopping_rounds=early_stopping_rounds, verbose_eval=True)
        # train results
        score = bst.best_score
        print('Best score: {:.6g}'.format(score))
        fold_score.append(score)
        importance = sorted(bst.get_fscore().items(), key=itemgetter(1), reverse=1) # importance := bst.get_fscore()
        print('Features importance :\n{}'.format(importance))
        print('Number of features:', len(feats))
        print('Features\n',feats)
        # predict on test
        fin_pred = bst.predict(xgb.DMatrix(test[feats].as_matrix()), ntree_limit=bst.best_ntree_limit)


    all_score_table = pd.DataFrame(scores,columns=['eta','max_depth','mean_score','std_score'])
    print ("All scores: \n", all_score_table) # choose the best model wrt params
    print('Best parameters:', params)
    print('Training time: {} min'.format(round((time.time() - t0)/60, 3)))

    # # Save model:
    # bst.save_model('latest.model')
    # bst.dump_model('latestdump.raw.txt','latestfeatmap.txt')

    # # Loading a model #have not tried
    # bst = xgb.Booster({'nthread':4}) #init model
    # bst.load_model("latestmodel.bin") # load data
    return fin_pred, mean_score
