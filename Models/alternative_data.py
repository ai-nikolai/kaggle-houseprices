import numpy as np
import pandas as pd
import csv,datetime
from sklearn.model_selection import train_test_split, KFold
import xgboost as xgb
from operator import itemgetter
import time
import matplotlib.pyplot as plt
import seaborn as sns
import itertools
from sklearn import preprocessing


from generic             import save_predictions
from data_clearing_v_1_0 import preprocess_feats


############################################################################
# Main code
###########################################################################
if __name__ ==  "__main__":

    train = pd.read_csv("../train.csv")
    test = pd.read_csv("../test.csv")
    train,test,feats,d1,d11,d2,d22,labels = preprocess_feats(train,test)






    # print(score)

    # plt.figure()
    # sns.heatmap(scores, annot=False, fmt="6.4f",cmap="YlGnBu")
    # # sns.heatmap(scores, annot=True, fmt="6.4f",cmap="YlGnBu")
    # #
    # # plt.figure()
    # # sns.heatmap(np.mean(scores,1), annot=False, fmt="6.4f",cmap="YlGnBu")
    # # # sns.heatmap(scores, annot=True, fmt="6.4f",cmap="YlGnBu")
    # #
    # # plt.figure()
    # # sns.heatmap(np.mean(scores,2), annot=False, fmt="6.4f",cmap="YlGnBu")
    # # # sns.heatmap(scores, annot=True, fmt="6.4f",cmap="YlGnBu")
    #
    # plt.show()
